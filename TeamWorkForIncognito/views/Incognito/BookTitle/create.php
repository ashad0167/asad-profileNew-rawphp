

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        body{
            background: url("c1.jpg") no-repeat;
            background-size: 100%;

        }

        .information{

            background-color:#8a6d3b;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            align-content: center;
            align-items: center;
            alignment: center;

            border: solid;


        }

        .main{
            align-content: center;
            align-items: center;
            alignment: center;
            width:500px;
            height: 300px;
            display: inline-block;


        }

        h1{
            color: white;
        }

    </style>

</head>
<body>
<div class="container">

    <div class="navbar">
        <td><a href='../../../../index.html' class='btn btn-group-lg btn-info'>Home</a> </td>
        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>

    <center>
        <div class="main">





            <h1>Book Title</h1><br>
            <div class="information">


    <form  class="form-group f" action="store.php" method="post">

      <h3>  Enter Book Name:</h3>
        <input class="form-control" type="text" name="bookName">
        <br>
       <h3> Enter Author Name:</h3>
        <input class="form-control" type="text" name="authorName">
        <br>
        <input type="submit" class='btn btn-group-lg btn-info' value="Enter">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


