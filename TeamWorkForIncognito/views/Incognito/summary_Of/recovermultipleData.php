<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use App\SummaryOfCompany\Summary;

if(isset($_POST['mark'])) {

    $obj = new summary();
    $obj->recoverMultipleDt($_POST['mark']);

    Utility::redirect("ViewAllCompanyData.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashedData.php");

}