<?php
require_once("../../../vendor/autoload.php");

$email = new \App\Email\Email();
$email->setData($_GET);
$oneData = $email->getSingleDataRecords();

use App\Message\Message;


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email-Single Data</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("email1.jpg") no-repeat;
            background-size: 100%;

        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center";">Detail</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Email</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->email</td>
                  </tr>
              ";

        ?>

    </table>

</div>

</body>
</html>