

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";


$email = new \App\Email\Email();
$email->setData($_GET);
$oneData = $email->getSingleDataRecords();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Page</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<style>

    body{
        background: url("email1.jpg") no-repeat;
        background-size: 100%;

    }

    .information{

        background-color:#9ccff4;
        color: #fff;
        font-weight: bold;
        padding: 10px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        align-content: center;
        align-items: center;
        alignment: center;

        border: solid;


    }

    .main{
        align-content: center;
        align-items: center;
        alignment: center;
        width:500px;
        height: 300px;
        display: inline-block;


    }

</style>
</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='ViewAllData.php' class='btn btn-group-lg btn-info'>All Data</a> </td>

    </div>



    <center>
        <div class="main">





            <h1>Email Edit</h1><br>
            <div class="information">


    <form  class="form-group" action="UpdateFromEditFile.php" method="post">

        <h3>  Name :</h3>
        <input class="form-control" type="text" name="personName" value="<?php echo $oneData->name?>">
        <br>
        <h3> Email :</h3>
        <input class="form-control" type="text" name="personEmai" value="<?php echo $oneData->email?>">
        <br>
        <input type="hidden"  name="id" value="<?php echo $oneData->id?>">
        <input type="submit" class='btn btn-group-lg btn-info' value="ENTER">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


