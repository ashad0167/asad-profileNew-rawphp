<?php
require_once("../../../vendor/autoload.php");

$objAddGender = new \App\AddGender\AddGender();
$objAddGender->setData($_GET);
$oneData = $objAddGender->view();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Gender - Single Information</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("gender2.jpg") no-repeat;
            background-size: 100%;

        }
        h1{

            color: white;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Add Gender - Single  Information</h1>


    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Applicant's Name</th>
            <th>Gender</th>
            <th>Action Button</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->gender</td>
                     <td><a href='index.php' class='btn btn-info'>Back To Active List</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>

</body>
</html>