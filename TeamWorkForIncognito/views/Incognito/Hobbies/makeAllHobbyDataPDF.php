<?php
include_once ('../../../vendor/autoload.php');
use App\Email\Email;

$obj= new \App\Hobby\Hobbies();
 $recordSet=$obj->getAllRecords();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($recordSet as $row) {
        $id =  $row->id;
        $name = $row->name;
        $hobby =$row->hobby;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='80'> $sl</td>";
        $trs .= "<td width='80'> $id </td>";
        $trs .= "<td width='250'> $name </td>";
        $trs .= "<td width='250'> $hobby </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<h3 align="center"> All Data Records </h3>
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Name</th>
                    <th align='left' >Hobbies</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');