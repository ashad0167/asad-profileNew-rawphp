<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
session_start();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birth Day -  Birth day Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("c3.jpg") no-repeat;
            background-size: 100%;

        }



        h1{
            color: white;
        }
    </style>



</head>
<body>



<div class="container">



<?php

if(isset($_POST['mark']) || isset($_SESSION['mark'])) {    // start of boss if
   $someData=null;
   $objBirthDay= new App\BirthDay\BirthDay();


   if(isset($_POST['mark']) ){
    $_SESSION['mark'] = $_POST['mark'];
    $someData =  $objBirthDay->listSelectedData($_SESSION['mark']);
   }
    echo "
          <h1> Are you sure you want to delete all selected data?</h1>
          <a href='deletemultiplebirthday.php?YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='indexbirthday.php' class='btn btn-success'>No</a>
        ";


    if(isset($_GET['YesButton'])){
        $objBirthDay->deleteMultiple($_SESSION['mark']);
        unset($_SESSION['mark']);
    }
?>


    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>


            <th style='width: 10%; text-align: center'>Serial Number</th>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Person Name</th>
            <th>Birth Day</th>
        </tr>

        <?php
        $serial = 1;


        foreach ($someData as $oneData) { ########### Traversing $someData is Required for pagination  #############

            if ($serial % 2) $bgColor = "AZURE";
            else $bgColor = "#ffffff";

            echo "

                  <tr  style='background-color: $bgColor'>


                     <td style='width: 10%; text-align: center'>$serial</td>
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->p_name</td>
                     <td>$oneData->dob</td>


                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

<?php
}  // end of boos if
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect($_SERVER["HTTP_REFERER"]);
}


?>


</div>


</body>
</html>