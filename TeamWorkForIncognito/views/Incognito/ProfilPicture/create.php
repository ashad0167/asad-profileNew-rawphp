

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture-Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>
        body{
            background: url("profile2.jpg") no-repeat;
            background-size: 100%;

        }

        .information{

            background-color:#1b6d85;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            align-content: center;
            align-items: center;
            alignment: center;

            border: solid;


        }

        .main{
            align-content: center;
            align-items: center;
            alignment: center;
            width:500px;
            height: 300px;
            display: inline-block;


        }

        h1{
            color: #1b6d85;
        }

    </style>

</head>
<body>
<div class="container">

    <div class="navbar">
        <td><a href='../../../../index.html' class='btn btn-group-lg btn-info'>Home</a> </td>
        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>

    <center>
        <div class="main">





            <h1>Profile Picture</h1><br>
            <div class="information">
 <form  class="form-group f" action="store.php" method="post" enctype="multipart/form-data">

    <h3>  Name:</h3>
     <input class="form-control" type="text" name="name" placeholder="Enter Name Here...">
     <br>
     <br>
      <h3>Photo:</h3>
     <input class="form-control" type='file' name= 'image'>
     <br>
     <br>
     <input type="submit" value="Upload"class='btn btn-group-lg btn-info'>

 </form>

      </div>


 <script src="../../../resource/bootstrap/js/jquery-ui.js"></script>

 <script>
     jQuery(

         function($) {
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
         }
     )
 </script>

</div>
            </center>

</body>
</html>










